﻿using System.Collections.Generic;
using cc_backend_engineering_test.Enums;

namespace cc_backend_engineering_test.Models
{
    /**
     * Represents a Tic-Tac-Toe board
     */
    public class Board
    {
        public int GameId { get; set; }
        public List<Cell> GameBoard { get; set; }
        public Player CurrentPlayer { get; set; }

        public Board()
        {
            CurrentPlayer = Player.X;

            GameBoard = new List<Cell>();

            for (int i = 0; i <= 8; i++)
            {
                Cell cell = new Cell
                {
                    X = i / 3,
                    Y = i % 3
                };

                GameBoard.Add(cell);
            }
        }
    }
}
