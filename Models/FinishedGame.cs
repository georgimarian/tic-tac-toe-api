﻿using cc_backend_engineering_test.Enums;

namespace cc_backend_engineering_test.Models
{
    /**
     * Game object used for statistics
     */
    public class FinishedGame
    {
        public int GameId { get; set; }
        public Player Winner { get; set; }

    }
}
