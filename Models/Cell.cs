﻿using cc_backend_engineering_test.Enums;

namespace cc_backend_engineering_test.Models
{
    /**
     * Represents a cell in the Tic-Tac-Toe board
     */
    public class Cell
    {
        public int X { get; set; }
        public int Y { get; set; }
        public Player Value { get; set; }

        public Cell()
        {
            Value = Player.Unassigned;
        }
    }
}
