﻿using System;
using System.IO;
using System.Linq;
using cc_backend_engineering_test.Enums;
using cc_backend_engineering_test.Models;

namespace cc_backend_engineering_test.Helpers
{
    /**
     * Deals with file reading and writing
     */
    public class FileHelper
    {
        private static string folder = @"C:\Users\georg\source\repos\cc-backend-engineering-test\";
        private static string fileName = "board.txt";

        private static string  fullPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, fileName);

        /**
         * Writes a board into a file called "board.txt"
         */
        public void WriteFile(Board board)
        {
            using (TextWriter textWriter = new StreamWriter(fullPath, false))
            {
                textWriter.Write(board.GameId);
                textWriter.WriteLine();

                textWriter.Write(board.CurrentPlayer);
                textWriter.WriteLine();

                for (int i = 0; i <= 2; i++)
                {
                    for (int j = 0; j <= 2; j++)
                    {
                        textWriter.Write(board.GameBoard[i * 3 + j].Value + " ");
                    }

                    textWriter.WriteLine();
                }
            }
        }

        /**
         * "Reads a board from a file called "board.txt"
         */
        public Board ReadFile()
        {
            string[] readText = File.ReadAllText(fullPath)
                .Split(new Char[] { ' ', '\\', '\n', '\r' },
                    StringSplitOptions.RemoveEmptyEntries);

            Board board = new Board
            {
                GameId = Int32.Parse(readText[0]),
                CurrentPlayer = GetPlayer(readText[1])
            };

            for (int i = 0; i <= 8; i++)
            {
                board.GameBoard[i].Value = GetPlayer(readText[i + 2]);
            }

            return board;
        }

        /**
         * Gets player as enum value from string
         */
        public Player GetPlayer(string player)
        {
            if (player == "Unassigned")
            {
                return Player.Unassigned;
            }

            if (player == "X")
            {
                return Player.X;
            }

            return Player.O;
        }
    }
}
