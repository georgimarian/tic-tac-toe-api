﻿using System;
using System.Collections.Generic;
using System.IO;
using cc_backend_engineering_test.Models;
using Newtonsoft.Json;
using JsonSerializer = Newtonsoft.Json.JsonSerializer;

namespace cc_backend_engineering_test.Helpers
{
    /**
     * Write and read data from JSON file
     */
    public static class JsonHelper
    {
        private static string folder = @"C:\Users\georg\source\repos\cc-backend-engineering-test\";
        private static string file = "statistics.txt";
        private static string fileName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, file);
        public static void Serialize(List<FinishedGame> games)
        {
            using StreamWriter file = File.CreateText(fileName);

            JsonSerializer serializer = new JsonSerializer();
            serializer.Serialize(file, games);
        }

        public static List<FinishedGame> Deserialize()
        {
            var entityData = File.ReadAllText(fileName);
            List<FinishedGame> entities = JsonConvert.DeserializeObject<List<FinishedGame>>(entityData);

            return entities;
        }


        public static void AddGame(FinishedGame entityToAdd)
        {
            List<FinishedGame> finishedGames;

            try
            {
                finishedGames = Deserialize();
            }
            catch (FileNotFoundException e)
            {
                finishedGames = new List<FinishedGame>();
            }

            finishedGames.Add(entityToAdd);

            Serialize(finishedGames);
        }
    }
}