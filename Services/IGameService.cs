﻿using cc_backend_engineering_test.Enums;
using cc_backend_engineering_test.Models;

namespace cc_backend_engineering_test.Services
{
    public interface IGameService
    {
        Board GenerateGame();

        Board Reset(string id);

        bool IsGameCompleted(Board board);

        Board Move(int x, int y);

        Board GetCurrentGame();

        Player EvaluateWinningBoard(Board board);

        bool IsDraw(Board board);

        string GetStatistics();

        void SerializeGame(int id, Player winnerPlayer);

    }
}
