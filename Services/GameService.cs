﻿using System;
using System.Collections.Generic;
using cc_backend_engineering_test.Enums;
using cc_backend_engineering_test.Helpers;
using cc_backend_engineering_test.Models;

namespace cc_backend_engineering_test.Services
{
    public class GameService : IGameService
    {
        private readonly FileHelper fileHelper;
        public GameService()
        {
            fileHelper = new FileHelper();
        }

        /**
         * Generates a game with a random ID and an empty board
         */
        public Board GenerateGame()
        {
            var randomNumberGenerator = new Random();

            Board gameBoard = Reset(randomNumberGenerator.Next(100, 2000).ToString());

            return gameBoard;
        }

        /**
         * Clears the board, but keeps the game ID
         */
        public Board Reset(string id)
        {
            Board board = new Board();
            board.GameId = Int32.Parse(id);
            fileHelper.WriteFile(board);

            return board;
        }

        /**
         * Read current game state
         */
        public Board GetCurrentGame()
        {
            return fileHelper.ReadFile();
        }

        /**
         * Moves an X or 0 on the board if there is available space
         */
        public Board Move(int x, int y)
        {
            Board board = fileHelper.ReadFile();

            if (!IsGameCompleted(board) && CanMove(x, y, board))
            {
                board.GameBoard[x * 3 + y].Value = board.CurrentPlayer;
                SwitchPlayers(board);
                fileHelper.WriteFile(board);

                return board;
            }

            return null;
        }

        /**
         * Validates cell for move
         */
        public bool CanMove(int x, int y, Board board)
        {
            return board.GameBoard[x * 3 + y].Value == Player.Unassigned;
        }

        /**
         * Switch players' turns
         */
        public void SwitchPlayers(Board board)
        {
            board.CurrentPlayer = board.CurrentPlayer == Player.X ? Player.O : Player.X;
        }

        /**
         * Check if game is completed
         */
        public bool IsGameCompleted(Board board)
        {
            return EvaluateWinningBoard(board) != Player.Unassigned || IsDraw(board);
        }

        /**
         * Check if game result is a draw
         */
        public bool IsDraw(Board board)
        {
            int unassignedCount = 0;

            for (int i = 0; i <= 2; i++)
            {
                for (int j = 0; j <= 2; j++)
                {
                    if (board.GameBoard[i * 3 + j].Value == Player.Unassigned)
                        unassignedCount++;
                }
            }
            return unassignedCount == 0;
        }

        /**
         * Evaluate if there is a winner player on the board
         */
        public Player EvaluateWinningBoard(Board board)
        {
            Player mainDiagonal = CheckMainDiagonal(board);
            Player secondaryDiagonal = CheckSecondaryDiagonal(board);
            Player line = CheckLines(board);
            Player column = CheckColumns(board);

            if (mainDiagonal != Player.Unassigned)
                return mainDiagonal;
            if (secondaryDiagonal != Player.Unassigned)
                return secondaryDiagonal;
            if (line != Player.Unassigned)
                return line;
            if (column != Player.Unassigned)
                return column;
            return Player.Unassigned;
        }

        /**
         * Check board lines for winner
         */
        public Player CheckLines(Board board)
        {
            for (int i = 0; i <= 2; i++)
            {
                Player currentLineWinner = CheckLine(i, board);

                if (currentLineWinner != Player.Unassigned)
                {
                    return currentLineWinner;
                }
            }
            return Player.Unassigned;
        }

        /**
         * Check board columns for winner
         */
        public Player CheckColumns(Board board)
        {
            for (int i = 0; i <= 2; i++)
            {
                Player currentLineWinner = CheckColumn(i, board);

                if (currentLineWinner != Player.Unassigned)
                {
                    return currentLineWinner;
                }
            }
            return Player.Unassigned;
        }

        /**
         * Check a board line for winner
         */
        public Player CheckLine(int line, Board board)
        {
            Player potentialWinner = board.GameBoard[line].Value;

            for (int y = 1; y <= 2; y++)
            {
                if (board.GameBoard[line * 3 + y].Value != potentialWinner)
                {
                    return Player.Unassigned;
                }
            }

            return potentialWinner;
        }

        /**
         * Check a board column for winner
         */
        public Player CheckColumn(int column, Board board)
        {
            Player potentialWinner = board.GameBoard[column].Value;

            for (int x = 0; x <= 2; x++)
            {
                if (board.GameBoard[x * 3 + column].Value != potentialWinner)
                {
                    return Player.Unassigned;
                }
            }

            return potentialWinner;
        }

        /**
         * Check a board's main diagonal for winner
         */
        public Player CheckMainDiagonal(Board board)
        {
            Player potentialWinner = board.GameBoard[0].Value;

            for (int x = 0; x <= 2; x++)
            {
                if (board.GameBoard[x * 3 + x].Value != potentialWinner)
                {
                    return Player.Unassigned;
                }
            }

            return potentialWinner;
        }

        /**
         * Check a board's secondary diagonal for winner
         */
        public Player CheckSecondaryDiagonal(Board board)
        {
            Player potentialWinner = board.GameBoard[0].Value;

            for (int x = 0; x <= 2; x++)
            {
                for (int y = 0; y <= 2; y++)
                {
                    if (x + y == 2)
                    {
                        if (board.GameBoard[x * 3 + y].Value != potentialWinner)
                        {
                            return Player.Unassigned;
                        }
                    }
                }
            }

            return potentialWinner;
        }

        /**
         * Gets statistical information about games
         */
        public string GetStatistics()
        {
            List<FinishedGame> finishedGames = JsonHelper.Deserialize();

            int numberOfTies = 0,
                xWins = 0,
                oWins = 0;

            foreach (var game in finishedGames)
            {
                if (game.Winner == Player.Unassigned)
                {
                    numberOfTies++;
                }
                else if (game.Winner == Player.X)
                {
                    xWins++;
                }
                else oWins++;
            }

            return "Number of ties is: " + numberOfTies +
                   "\nGames won by X: " + xWins +
                   "\nGames won by O: " + oWins;
        }

        /**
         * Adds game in statistics file
         */
        public void SerializeGame(int id, Player winnerPlayer)
        {
            FinishedGame game = new FinishedGame
            {
                GameId = id,
                Winner = winnerPlayer
            };

            JsonHelper.AddGame(game);
        }
    }
}
