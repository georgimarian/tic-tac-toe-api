namespace cc_backend_engineering_test
{
    public partial class Program
    {
        public Program() { }
        public static Microsoft.Extensions.Hosting.IHostBuilder CreateHostBuilder(string[] args) { return default(Microsoft.Extensions.Hosting.IHostBuilder); }
        public static void Main(string[] args) { }
    }
    public partial class Startup
    {
        public Startup(Microsoft.Extensions.Configuration.IConfiguration configuration) { }
        public Microsoft.Extensions.Configuration.IConfiguration Configuration { [System.Runtime.CompilerServices.CompilerGeneratedAttribute]get { return default(Microsoft.Extensions.Configuration.IConfiguration); } }
        public void Configure(Microsoft.AspNetCore.Builder.IApplicationBuilder app, Microsoft.AspNetCore.Hosting.IWebHostEnvironment env) { }
        public void ConfigureServices(Microsoft.Extensions.DependencyInjection.IServiceCollection services) { }
    }
}
namespace cc_backend_engineering_test.Controllers
{
    [Microsoft.AspNetCore.Mvc.ApiControllerAttribute]
    [Microsoft.AspNetCore.Mvc.RouteAttribute("[controller]")]
    public partial class GameController : Microsoft.AspNetCore.Mvc.ControllerBase
    {
        public GameController(cc_backend_engineering_test.Services.IGameService gameService) { }
        [Microsoft.AspNetCore.Mvc.HttpGetAttribute("{id}")]
        public Microsoft.AspNetCore.Mvc.IActionResult GetCurrentGame(string id) { return default(Microsoft.AspNetCore.Mvc.IActionResult); }
        [Microsoft.AspNetCore.Mvc.HttpGetAttribute("newGame")]
        public Microsoft.AspNetCore.Mvc.IActionResult GetNewGame() { return default(Microsoft.AspNetCore.Mvc.IActionResult); }
        [Microsoft.AspNetCore.Mvc.HttpGetAttribute("stats")]
        public Microsoft.AspNetCore.Mvc.IActionResult GetStatistics() { return default(Microsoft.AspNetCore.Mvc.IActionResult); }
        [Microsoft.AspNetCore.Mvc.HttpGetAttribute("{id}/reset")]
        public Microsoft.AspNetCore.Mvc.IActionResult ResetGame(string id) { return default(Microsoft.AspNetCore.Mvc.IActionResult); }
        [Microsoft.AspNetCore.Mvc.HttpPostAttribute("{id}")]
        public Microsoft.AspNetCore.Mvc.IActionResult SetCell([Microsoft.AspNetCore.Mvc.FromBodyAttribute]cc_backend_engineering_test.DTOs.CellDto cell) { return default(Microsoft.AspNetCore.Mvc.IActionResult); }
    }
}
namespace cc_backend_engineering_test.DTOs
{
    public partial class CellDto
    {
        public CellDto() { }
        public int X { [System.Runtime.CompilerServices.CompilerGeneratedAttribute]get { return default(int); } [System.Runtime.CompilerServices.CompilerGeneratedAttribute]set { } }
        public int Y { [System.Runtime.CompilerServices.CompilerGeneratedAttribute]get { return default(int); } [System.Runtime.CompilerServices.CompilerGeneratedAttribute]set { } }
    }
}
namespace cc_backend_engineering_test.Enums
{
    public enum Player
    {
        O = 79,
        Unassigned = 95,
        X = 88,
    }
}
namespace cc_backend_engineering_test.Helpers
{
    public partial class FileHelper
    {
        public FileHelper() { }
        public cc_backend_engineering_test.Enums.Player GetPlayer(string player) { return default(cc_backend_engineering_test.Enums.Player); }
        public cc_backend_engineering_test.Models.Board ReadFile() { return default(cc_backend_engineering_test.Models.Board); }
        public void WriteFile(cc_backend_engineering_test.Models.Board board) { }
    }
    public static partial class JsonHelper
    {
        public static void AddGame(cc_backend_engineering_test.Models.FinishedGame entityToAdd) { }
        public static System.Collections.Generic.List<cc_backend_engineering_test.Models.FinishedGame> Deserialize() { return default(System.Collections.Generic.List<cc_backend_engineering_test.Models.FinishedGame>); }
        public static void Serialize(System.Collections.Generic.List<cc_backend_engineering_test.Models.FinishedGame> games) { }
    }
}
namespace cc_backend_engineering_test.Models
{
    public partial class Board
    {
        public Board() { }
        public cc_backend_engineering_test.Enums.Player CurrentPlayer { [System.Runtime.CompilerServices.CompilerGeneratedAttribute]get { return default(cc_backend_engineering_test.Enums.Player); } [System.Runtime.CompilerServices.CompilerGeneratedAttribute]set { } }
        public System.Collections.Generic.List<cc_backend_engineering_test.Models.Cell> GameBoard { [System.Runtime.CompilerServices.CompilerGeneratedAttribute]get { return default(System.Collections.Generic.List<cc_backend_engineering_test.Models.Cell>); } [System.Runtime.CompilerServices.CompilerGeneratedAttribute]set { } }
        public int GameId { [System.Runtime.CompilerServices.CompilerGeneratedAttribute]get { return default(int); } [System.Runtime.CompilerServices.CompilerGeneratedAttribute]set { } }
    }
    public partial class Cell
    {
        public Cell() { }
        public cc_backend_engineering_test.Enums.Player Value { [System.Runtime.CompilerServices.CompilerGeneratedAttribute]get { return default(cc_backend_engineering_test.Enums.Player); } [System.Runtime.CompilerServices.CompilerGeneratedAttribute]set { } }
        public int X { [System.Runtime.CompilerServices.CompilerGeneratedAttribute]get { return default(int); } [System.Runtime.CompilerServices.CompilerGeneratedAttribute]set { } }
        public int Y { [System.Runtime.CompilerServices.CompilerGeneratedAttribute]get { return default(int); } [System.Runtime.CompilerServices.CompilerGeneratedAttribute]set { } }
    }
    public partial class FinishedGame
    {
        public FinishedGame() { }
        public int GameId { [System.Runtime.CompilerServices.CompilerGeneratedAttribute]get { return default(int); } [System.Runtime.CompilerServices.CompilerGeneratedAttribute]set { } }
        public cc_backend_engineering_test.Enums.Player Winner { [System.Runtime.CompilerServices.CompilerGeneratedAttribute]get { return default(cc_backend_engineering_test.Enums.Player); } [System.Runtime.CompilerServices.CompilerGeneratedAttribute]set { } }
    }
}
namespace cc_backend_engineering_test.Services
{
    public partial class GameService : cc_backend_engineering_test.Services.IGameService
    {
        public GameService() { }
        public bool CanMove(int x, int y, cc_backend_engineering_test.Models.Board board) { return default(bool); }
        public cc_backend_engineering_test.Enums.Player CheckColumn(int column, cc_backend_engineering_test.Models.Board board) { return default(cc_backend_engineering_test.Enums.Player); }
        public cc_backend_engineering_test.Enums.Player CheckColumns(cc_backend_engineering_test.Models.Board board) { return default(cc_backend_engineering_test.Enums.Player); }
        public cc_backend_engineering_test.Enums.Player CheckLine(int line, cc_backend_engineering_test.Models.Board board) { return default(cc_backend_engineering_test.Enums.Player); }
        public cc_backend_engineering_test.Enums.Player CheckLines(cc_backend_engineering_test.Models.Board board) { return default(cc_backend_engineering_test.Enums.Player); }
        public cc_backend_engineering_test.Enums.Player CheckMainDiagonal(cc_backend_engineering_test.Models.Board board) { return default(cc_backend_engineering_test.Enums.Player); }
        public cc_backend_engineering_test.Enums.Player CheckSecondaryDiagonal(cc_backend_engineering_test.Models.Board board) { return default(cc_backend_engineering_test.Enums.Player); }
        public cc_backend_engineering_test.Enums.Player EvaluateWinningBoard(cc_backend_engineering_test.Models.Board board) { return default(cc_backend_engineering_test.Enums.Player); }
        public cc_backend_engineering_test.Models.Board GenerateGame() { return default(cc_backend_engineering_test.Models.Board); }
        public cc_backend_engineering_test.Models.Board GetCurrentGame() { return default(cc_backend_engineering_test.Models.Board); }
        public string GetStatistics() { return default(string); }
        public bool IsDraw(cc_backend_engineering_test.Models.Board board) { return default(bool); }
        public bool IsGameCompleted(cc_backend_engineering_test.Models.Board board) { return default(bool); }
        public cc_backend_engineering_test.Models.Board Move(int x, int y) { return default(cc_backend_engineering_test.Models.Board); }
        public cc_backend_engineering_test.Models.Board Reset(string id) { return default(cc_backend_engineering_test.Models.Board); }
        public void SerializeGame(int id, cc_backend_engineering_test.Enums.Player winnerPlayer) { }
        public void SwitchPlayers(cc_backend_engineering_test.Models.Board board) { }
    }
    public partial interface IGameService
    {
        cc_backend_engineering_test.Enums.Player EvaluateWinningBoard(cc_backend_engineering_test.Models.Board board);
        cc_backend_engineering_test.Models.Board GenerateGame();
        cc_backend_engineering_test.Models.Board GetCurrentGame();
        string GetStatistics();
        bool IsDraw(cc_backend_engineering_test.Models.Board board);
        bool IsGameCompleted(cc_backend_engineering_test.Models.Board board);
        cc_backend_engineering_test.Models.Board Move(int x, int y);
        cc_backend_engineering_test.Models.Board Reset(string id);
        void SerializeGame(int id, cc_backend_engineering_test.Enums.Player winnerPlayer);
    }
}
