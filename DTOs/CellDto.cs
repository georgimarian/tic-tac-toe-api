﻿
namespace cc_backend_engineering_test.DTOs
{
    /**
    * DTO for getting coordinates of move from request
    */

    public class CellDto
    {
        public int X { get; set; }
        public int Y { get; set; }
    }
}
