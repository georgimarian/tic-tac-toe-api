﻿namespace cc_backend_engineering_test.Enums
{
    /**
     * Enumeration for cell values
     */
    public enum Player
    {
        X = 'X',
        O = 'O',
        Unassigned = '_'
    }
}
