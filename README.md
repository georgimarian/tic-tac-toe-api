# cc-backend-engineering-test
Engineering Test for Edwards

Georgiana Marian

25.06.2020

Instructions

The following project implements an API for a 2-player Tic-Tac-Toe game. 

I have considered only one active game at a time and the two players being X and O.
The application has no database and stores the current board state in a .txt file.
Information about each game's winner can be found in "statistics.txt" file.

To start a new game, one accesses
https://localhost:5001/game/newGame
which yields the new game ID and sets the board with blank spaces.

The game begins with X's move and ends when either X or O win or we have a tie. 
To make a move, POST to 
https://localhost:5001/game/{id} 
having a body of the form
{
"x": numericValue,
"y": numericValue
}

To get the current game state, access
https://localhost:5001/game/{id} 

The game can be reset at any time, provided that a game with a given ID already exists
https://localhost:5001/game/{id}/reset

Statistics about the game winners can be found at 
https://localhost:5001/game/stats


