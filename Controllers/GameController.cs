﻿using System;
using cc_backend_engineering_test.DTOs;
using cc_backend_engineering_test.Enums;
using cc_backend_engineering_test.Helpers;
using cc_backend_engineering_test.Models;
using cc_backend_engineering_test.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace cc_backend_engineering_test.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class GameController : ControllerBase
    {

        private readonly IGameService gameService;

        public GameController(IGameService gameService)
        {
            this.gameService = gameService;
        }

        [HttpGet("newGame")]
        public IActionResult GetNewGame()
        {
            Board gameBoard = gameService.GenerateGame();

            return Ok("The game id is: " + gameBoard.GameId + ". \n"
                      + "You can start a new game at https://localhost:5001/game/" + gameBoard.GameId);
        }

        [HttpGet("{id}")]
        public IActionResult GetCurrentGame(string id)
        {
            Board board = gameService.GetCurrentGame();

            if (board.GameId == Int32.Parse(id))
            {
                return Ok(board);
            }

            return BadRequest($"Game with id {id} does not exist");
        }

        [HttpGet("{id}/reset")]
        public IActionResult ResetGame(string id)
        {
            Board board = gameService.GetCurrentGame();

            if (board.GameId == Int32.Parse(id))
            {
                board = gameService.Reset(id);
                return Ok("Game has been reset.");
            }

            return BadRequest($"Game with id {id} does not exist");
        }

        [HttpPost("{id}")]
        public IActionResult SetCell([FromBody] CellDto cell)
        {
            Board moved = gameService.Move(cell.X, cell.Y);

            if (moved == null)
            {
                return BadRequest("Cannot move to an occupied cell!");
            }

            if (gameService.IsGameCompleted(moved))
            {
                if (gameService.IsDraw(moved))
                {
                    return Ok("The result has been a tie!");
                }

                Player winnerPlayer = gameService.EvaluateWinningBoard(moved);
                gameService.SerializeGame(moved.GameId, winnerPlayer);

                return Ok("The game has been won by " + winnerPlayer);
            }
            return Ok(moved);
        }

        [HttpGet("stats")]
        public IActionResult GetStatistics()
        {
            string message = gameService.GetStatistics();

            return Ok(message);
        }
    }
}
